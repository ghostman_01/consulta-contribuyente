
(function ($) {
    "use strict";
    var todosLosPredios;
    
    $('.validate-form').on('submit',function(e){
        e.preventDefault();
        var _user = $("#codigo").val();
        var _password = $("#dni").val();
        console.log(_user);
        console.log(_password);

        if (_user =="" || _password =="" || _user == null || _password == null) {

            $("#mensaje-validation").text("Mensaje: Debe llenar todo el formulario.");
            $("#mensaje-validation").show();

        }else{
            toogleLoading(true);
            $("#mensaje-validation").text("");
            $("#mensaje-validation").hide();

            $.ajax({
                type: 'POST',
                url: 'http://177.91.254.134/munisegura/api/wp-web/login',// RUTA Q VALIDA AL USUARIO
                contentType: "application/json",
                dataType: "json",
                cache: false,
                data: JSON.stringify({ user: _user, password: _password }),
                success: function(res){
                    // console.log(res);
                    if (res.response.message == "correcto") {
                        //PETICION PARA TRAER DATA:
                        $.ajax({
                            type: 'POST',
                            url: 'http://177.91.254.134/munisegura/api/wp-web/get-data',// RUTA Q VALIDA AL USUARIO
                            contentType: "application/json",
                            dataType: "json",
                            cache: false,
                            data: JSON.stringify({ user: _user, password: _password }),
                            success: function(data){
                                toogleLoading(false);
                                //console.log(JSON.parse(data.response.message));
                                var string_info = data.response.message;
                                var info = JSON.parse(string_info);
                                console.log(info);

                                //ASIGNACION DE INFORMACION - CONTRIBUYENTE
                                $("#data-codigo-contri").text(info[0][1]);
                                $("#data-name").text(info[0][4]);
                                $("#data-doc-name").text(capitalizeOnlyFirst(info[0][2])+":");
                                $("#data-doc-number").text(info[0][3]);
                                $("#data-telefono").text(info[0][9]);
                                $("#data-correo").text(info[0][10]);
                                $("#data-genero").text(info[0][11]);
                                $("#data-departamento").text(info[0][5]);
                                $("#data-provincia").text(info[0][6]);
                                $("#data-distrito").text(info[0][7]);
                                $("#data-direccion").text(info[0][8]);
                                $("#data-estado").text(info[0][12]);
                                $("#data-codigo-conyuge").text(info[0][13]);
                                $("#data-name-conyuge").text(info[0][14]);

                                //ASIGNACION DE INFORMACION - PREDIO
                                    //PINTADO DE PREDIOS EN EL HTML
                                    var cant = info.length;
                                    $("#predio").empty();
                                    for (var i = 0, limit = cant; i < limit; i++) {
                                        var predios =info[i][15];
                                        console.log(predios);
                                        $("#predio").append('<option value='+i+'>'+predios+'</option>');
                                    }
                                  
                                    //INICIAMOS CON EL PRIMER PREDIO.
                                    todosLosPredios = info;
                                    pintarInformacion(todosLosPredios, 0)

                                    $(".data-user").hide("slow");
                                    $(".data-contribuyente").show("slow");

                                  
                            },
                            error:function(e){
                                toogleLoading(false);
                                console.log(e);

                            }
                        });

                    }else{
                        $("#mensaje-validation").text("Mensaje: Algo inesperado ocurri贸, intentalo mas tarde.");
                        $("#mensaje-validation").show();
                        toogleLoading(false);
                    }
                        
                    
                },
                error:function(err){ //yap estoy atenta
    
                    //CUANDO ES DIFERENTE A STATUS==200
                    console.log("error encontrado.");
                    console.log(err);
                    $("#mensaje-validation").text("Mensaje: "+ err.responseJSON.response +"");
                    $("#mensaje-validation").show();
                    toogleLoading(false);
                }
                    
            });
        }
        
    });
    



    //PARA VOLVER AL LOGIN
    $('.volver').on('submit',function(e){
        e.preventDefault();
        $(".data-contribuyente").hide("slow");
        $(".data-user").show("slow");
        //pintarInformacion();
    });


    //CUANDO CAMBIA DE VALOR EL COMBO (SELECT)
    $('select').on('change', function() {
      // alert( this.value );
      pintarInformacion(todosLosPredios, this.value);
    });

    //FUNCION DE APOYO.

    function toogleLoading(status = false){
        if (status) {
            $("#btn-consultar").hide();
            $("#img-loading").show();    
        }else{
            $("#img-loading").hide();
            $("#btn-consultar").show();
        }
    }


    /*
    *@Description: Funcion que pinta la informaci贸n
    *@Inputs: arreglo y valor de "a" (el arreglo es bidimensional y "a" es el indice de filas.)
    *@Outpus: sin salida, manipula el DOM (HTML)
    */
    function pintarInformacion(arreglo, indice){
        var data = arreglo;
        var a = indice;
        if (data) {
            $("#data-codigo-predio").text(data[a][15]);
            $("#data-tipo-predio").text(data[a][17]);
            $("#data-uso-predio").text(data[a][18]);
            $("#data-condicion-predio").text(data[a][19]);
            $("#data-frontis").text(data[a][20]);
            $("#data-area-terreno").text(data[a][21]);
            $("#data-area-construida").text(data[a][22]);
            $("#data-zona").text(data[a][23]);
            $("#data-urbanizacion").text(data[a][24]);
            $("#data-calle").text(data[a][25]);
            $("#data-manzana").text(data[a][26]);
            $("#data-interior").text(data[a][27]);
            $("#data-bloque").text(data[a][28]);
            $("#data-numero").text(data[a][29]);
            $("#data-lote").text(data[a][30]);
            $("#data-cuadra").text(data[a][31]);
            $("#data-sector").text(data[a][32]);
            $("#data-ubicacion").text(data[a][33]);
            $("#data-observacion").text(data[a][34]);
        }
        
    }

    function capitalizeOnlyFirst(string){
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

})(jQuery);

